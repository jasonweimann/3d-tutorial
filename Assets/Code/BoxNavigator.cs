﻿using UnityEngine;

public class BoxNavigator : MonoBehaviour
{
	private NavMeshAgent _navMeshAgent;

	private void Start()
	{
		// Find the Nav Mesh Agent component on this gameobject and save a reference to it.
		_navMeshAgent = GetComponent<NavMeshAgent>();
	}
	
	// Update is called once per frame
	private void Update()
	{
		// Left click is bound to the Fire1 button - Here we're checking for a click (touch also counts)
		if (Input.GetButton("Fire1"))
		{
			var mouseClickedPositionOnScreen = Input.mousePosition; // Get the screen position of the mouse (or touch)

			// Create Ray starting from the main camera into the world.
			Ray ray = Camera.main.ScreenPointToRay(mouseClickedPositionOnScreen);

			RaycastHit hitInfo; // We'll store the info on where the player clicked in hitInfo

			// Raycast will return true if the player clicked on any object with a collider in our scene (the plane)
			bool clickedGround = Physics.Raycast(ray, out hitInfo);

			if (clickedGround)
			{
				Vector3 pointOnGroundThatWasClicked = hitInfo.point; // Get the point on the plane where the player clicked
				SetMyDestination(pointOnGroundThatWasClicked); // Tell our Nav Mesh Agent where to go
			}

		}
	}

	private void SetMyDestination(Vector3 pointOnGroundThatWasClicked)
	{
		// This tells the Nav Mesh Agent where we'd like it to move our box
		_navMeshAgent.SetDestination(pointOnGroundThatWasClicked);
	}


	/// <summary>
	/// This is used after the box splits.
	/// Initially the boxes nav mesh agents are off.
	/// We wait until the boxes touch the ground to turn them on and disable physics.
	/// </summary>
	private void OnCollisionEnter(Collision collision)
	{
		if (collision.collider.gameObject.layer == LayerMask.NameToLayer("Ground"))
		{
			EnableNavMeshAgent();
		}
	}

	private void EnableNavMeshAgent()
	{
		// When enabled, the NavMeshAgent will be able to move the box toward the destination
		GetComponent<NavMeshAgent>().enabled = true;

		// IsKinematic prevents the box from being pushed by other physics objects or using gravity
		GetComponent<Rigidbody>().isKinematic = true;
	}
}
