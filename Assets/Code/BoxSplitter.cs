﻿using UnityEngine;

public class BoxSplitter : MonoBehaviour
{
	[SerializeField]
	private BoxSplitter _splitIntoPrefab;
	[SerializeField]
	private float _splitForce = 10f;

	private bool CanSplit { get { return _splitIntoPrefab != null; } }

	private void OnCollisionEnter(Collision collision)
	{
		if (collision.collider.gameObject.layer == LayerMask.NameToLayer("RedWeapon"))
		{
			Destroy(collision.collider.gameObject);

			if (CanSplit)
			{
				SplitInHalf();
			}
			Destroy(this.gameObject);
		}
	}

	private void SplitInHalf()
	{
		SpawnHalfCubeAndPushInDirection(Vector3.right);
		SpawnHalfCubeAndPushInDirection(Vector3.left);
	}

	private void SpawnHalfCubeAndPushInDirection(Vector3 direction)
	{
		var halfCube = Instantiate(_splitIntoPrefab, transform.position, transform.rotation) as BoxSplitter;
		halfCube.transform.localScale = transform.localScale * 0.5f;
		halfCube.GetComponent<NavMeshAgent>().enabled = false;
		halfCube.GetComponent<Rigidbody>().isKinematic = false;
		halfCube.GetComponent<Rigidbody>().AddForce(Vector3.up + direction * _splitForce);
	}
}