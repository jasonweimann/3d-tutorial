﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class RedBox : MonoBehaviour
{
	[SerializeField]
	private float _reloadRate = 2f;

	[SerializeField]
	private float _fireSpeed = 10f;

	[SerializeField]
	private Rigidbody _ammoPrefab;
	
	// Used to store the time until we reload/re-fire our weapon
	private float _timeUntilReloaded;

	void Update()
	{
		_timeUntilReloaded += Time.deltaTime;

		if (_timeUntilReloaded >= _reloadRate) // Is Reloaded?
		{
			FireWeapon();
		}

		if (ThisHasFallen())
			SceneManager.LoadScene(0);

		if (AllBlueBoxesDead())
			SceneManager.LoadScene(0);
	}

	private bool AllBlueBoxesDead()
	{
		return FindObjectOfType<BoxNavigator>() == null;
	}

	private bool ThisHasFallen()
	{
		return transform.position.y < 0f;
	}

	private void FireWeapon()
	{
		_timeUntilReloaded = 0f; // Reset the Fire Timer

		// Spawn the ammo
		var ammo = Instantiate(_ammoPrefab, transform.position, transform.rotation) as Rigidbody;

		// Get the direction to the blue box
		Vector3 directonToBlueBox = FindObjectOfType<BoxSplitter>().transform.position - transform.position;

		// Add force in that direction.  Notice we multiply by _fireSpeed so it's adjustable in the editor.
		ammo.AddForce(directonToBlueBox * _fireSpeed);
	}
}